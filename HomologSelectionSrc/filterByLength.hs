import Tables (extractSequ)
import System.Environment (getArgs)
import Data.List (nubBy)

main = do
  [in1,in2,out1] <- getArgs
  seqsTemp <- fmap lines $ readFile in1 -- MSA file (not including query)
  refTemp  <- fmap lines $ readFile in2 -- Fasta file of query (ie single seq)
  let	ts  = extractSequ seqsTemp -- returns a list of [(seq,header),(seq,header)...]
	[t] = extractSequ refTemp 
  	seqdata = secondFilter t ts 
  putStrLn $ "There are " ++ show (length seqdata) ++ "/" ++ show (length ts) ++ " seqs remaining" 
  writeFile out1 $ unlines $ map printOutput seqdata

printOutput (sq,nm) = nm ++ "\n" ++ sq ++ "\n"   

-- Discard from the MSA all sequences that are >3/2 or <2/3 the length of the query
secondFilter ref rest = filter (\(z,_) -> (sf z < 1.5*len) && (sf z > 0.66667*len)) rest
	where len = fromIntegral . length . fst $ ref
	      sf  = fromIntegral.length
