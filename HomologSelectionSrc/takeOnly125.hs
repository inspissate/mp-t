import Tables (extractSequ)
import System.Environment (getArgs)
import Data.List (nubBy,sort)
import Data.HashTable (hashString)

main = do
  [in1,out1,num,exclude] <- getArgs
  seqsTemp <- fmap lines $ readFile in1
  let	temps = extractSequ seqsTemp
  	care = read exclude :: Int 
	seqdata = take care temps ++ take ((read num :: Int)-care) (sortByHash (drop care temps))
  putStrLn $ "I see " ++ show (length temps) ++ " sequences"
  writeFile out1 $ unlines $ map printOutput seqdata

printOutput (sq,nm) = nm ++ "\n" ++ sq ++ "\n" 

sortByHash named = snd.unzip.sort $ zip (hashAll named) (named)
	where hashMe (_,x) = hashString x
	      hashAll      = map hashMe
  
