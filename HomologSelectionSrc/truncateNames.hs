import System.Environment (getArgs)

main = do
  [ins,out] <- getArgs
  inh <- fmap lines $ readFile ins
  writeFile out $ unlines $ map process inh

process line = if take 1  line == ">" then head . words $ line else line 
