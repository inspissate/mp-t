import Tables (seekField)
import System.Environment (getArgs)

main = do
  [inFile,outFile] <- getArgs
  temLines <- fmap lines $ readFile inFile
  -- The sequence is the first field that has the word "structure" as part of its label
  -- We wish to strip the asterisk from the end of the field
  let sequence = filter (/= '*') $ unlines $  seekField "structure" temLines
  -- Add the title back in as the first line
  writeFile outFile $ (temLines!!0) ++ "\n" ++ sequence ++ "\n"
