import Tables (extractSequ)
import System.Environment (getArgs)
import Data.List (nubBy)

main = do
  [in1,in2,out] <- getArgs
  seqsTemp <- fmap lines $ readFile in1
  seqsTarg <- fmap lines $ readFile in2 
  let	temps = extractSequ seqsTemp
  	targs = extractSequ seqsTarg
  	seqdata = filterSpecial $ alternate temps targs
  putStrLn $ "There are " ++ show (length seqdata) ++ " seqs after combining homologs"
  writeFile out $ unlines $ map printOutput seqdata

printOutput (sq,nm) = nm ++ "\n" ++ sq ++ "\n"   

alternate a b = concat $ zipWith (\x y -> x:[y]) a b

filterSpecial = nb fst . nb snd
 	where nb f = nubBy (\x y -> (f x) == (f y)) 
 

