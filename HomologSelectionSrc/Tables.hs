-- This is a translation of the ThreadPair table code into the multiple
-- alignment program. It should be resilient to errors, and reproduce the
-- appropriate log-odds scores.

module Tables (createAeij,seekField,extractSequ,extractTitles,makeProfile,readEnv,SubMat) where
import Data.List
import qualified Data.Map as Map
import Data.Array.Unboxed

type SubMat = UArray (Int,Int,Int) Double

{- A file is presented as [String]. We want to:
1) Throw away all white space and lines beginning with a #
2) If an entry starts with a '>', treat the first word after it as a table name
   If the table name already exists, complain with table name
3) Then discard all white space and lines beginning with a # until a new '>' is seen
-}

createAeij :: [String] -> (UArray (Int,Int,Int) Int, SubMat, Map.Map String Int, Map.Map Char Int)
createAeij tables = case dupLabels labels of
        [] -> (logodds,odds,envList,aa2id)
        x  -> error $ "Found more than one copy of table(s) " ++ (show x)
    where (labels,numbs,aas) = process ([],[],[]) (findStart tables)
          freqs = listArray ((1,1,1),(length labels, length aas, length aas)) (concat $ concat numbs)
	  findStart = dropWhile (\x-> head x /= '>') . ignoreComments
          odds    = freqsToOdds freqs
          logodds = amap (\x-> round (30 * log(x)/log(2))) odds
          envList = Map.fromList $ concat $ map (\(x,y) -> map (\z -> (z,y)) x) $ zip labels [2..] -- First index is reserved for total tables
          aa2id   = Map.fromList $ zip aas [1..]

-- Take a table of frequencies, and return a table of odds
freqsToOdds :: SubMat -> SubMat
freqsToOdds freqs = listArray ((1,1,1),(envNum+1,aaNum,aaNum)) $
   [num_sum!(j,k) / denom_odds!j | j<- aas, k<- aas] ++
   [freqs!(i,j,k)/(sum_b_of_AEba!(i,k) * denom_odds!(j)) | i<- envs, j<- aas, k<- aas]	
	where
	  (envNum,aaNum,_) = snd $ bounds freqs
	  aas = [1..aaNum]
          envs= [1..envNum]
	  sum_aE_of_AEba   = [sum [freqs!(i,j,k) | i<- envs , k<- aas] | j<- aas]
	  sum_bE_of_AEba   = [sum [freqs!(i,j,k) | i<- envs , j<- aas] | k<- aas]
	  sum_b_of_AEba    = listArray ((1,1),(envNum,aaNum))
			       [sum [freqs!(i,j,k) | j<- aas] | i<- envs , k<- aas]
				:: UArray (Int,Int) Double
	  sum_all          = sum sum_aE_of_AEba :: Double
	  denom_odds       = listArray (1,aaNum) $ map (\x-> x/sum_all) sum_aE_of_AEba :: UArray Int Double
	  num_sum          = listArray ((1,1),(aaNum,aaNum))
				[sum [freqs!(i,j,k) | i<- envs] / sum_bE_of_AEba!!(k-1) | j<- aas, k<- aas]
				 :: UArray (Int,Int) Double


ignoreComments :: [String] -> [String]
ignoreComments = filter (\x -> head x /='#') . filter (not.null)

process :: ([[String]], [[[Double]]], String)
                        -> [String]
                        -> ([[String]], [[[Double]]], String)
process (labAcc,tabAcc,aas) (k:ks) = process ((label k):labAcc,(tab ks):tabAcc,aaNew) (newks ks)
    where footer = doStuff.stripFormatting.fst.takeOneEntry
	-- Table has form [String]
	  doStuff t = [(head i, map (\x-> read x :: Double) (tail i)) | i<- (map words t)]
          stripFormatting = ignoreComments . map (unwords.words)
	  takeOneEntry = span (\x -> head x /= '>') -- Returns (table, still-to-process) tuple
	  newks  = snd.takeOneEntry
          aas'   = concat $ map fst (footer ks)
	  aaNew  = case aas of
			[] -> case dupLabels aas' of
				[] -> aas'
				x  -> error $ "Found more than one copy of amino acid " ++ (show x)
			_  -> case aas' == aas of
		    		True  -> aas
		    		False -> error $ "Amino acid headings of tables " ++ (unwords (head labAcc)) ++ "\
			        		     \ and " ++ (unwords (label k)) ++ " are different."
          label  = words.tail
	  -- SANITY (checks there are as many numbers for each entry as AAs)
          sanity x = all (==length x) (map (length.snd) x)
	  tab z = case sanity (footer z) of
                    True  ->  map snd (footer z)
                    False ->  error $ "Number of entries doesn't equal (Num. amino acids)^2\
                                      \ in table " ++ (unwords (label k))
process (labAcc,tabAcc,aas) _ = (labAcc, tabAcc,aas)

dupLabels :: Eq a => [a] -> [a]
dupLabels labels = labels \\ (nub labels)

-- /// Functions that deal with extracting annotations \\\ --

-- Extracts ("Table name", "residue") pairs from tem file
makeProfile :: [String] -> [String] -> [String] -> [String]
makeProfile tem sofar (env:envs)
	| envs == [] = r sofar env
	| otherwise = makeProfile tem (r sofar env) envs
	   where
	     r sofar2 env2 = zipWith (++) sofar2 (readEnv env2 tem)
makeProfile _ _ [] = error "Parse error reading structure information from .tem file"

readEnv :: String -> [String] -> [String]
readEnv env tem  = case seekField env tem of
	[] -> error $ "Could not find annotation '" ++ env ++ "' in annotation file"
	_  -> map (\x -> [x]) (concat $ seekField env tem)

-- Extract sequences subject to 1) A length constraint 2) No seq should contain a non-standard AA
extractSequ :: [String] -> [(String,String)]
extractSequ seqFile = filtered
         where a = map (filter (\y-> y/='-' && y/='*' && y/='\r' && y/=' ')) $ map concat $ seekFields ">" seqFile
	       b = extractTitles seqFile        -- Get the names of the seqs
	       fi = fromIntegral.length :: [a] -> Double
	       standardLength = fi (head a)     -- Name of seq of interest (always first in the alignment)
	       c = zip a b			-- Filtered ensures no seq contains an x, or is the wrong length
	       filtered = filter (\(x,_) -> (not . any (\z-> (z=='X') || (z=='U') || (z=='Z') || (z=='B') || (z=='J') )) x) $ c
	
		
safeHead :: [t] -> [t]
safeHead (x:_) = [x]
safeHead [] = []

-- /// Functions that read part or more of a tem file \\\ --
extractTitles :: [String] -> [String]
extractTitles q = filter (\x-> safeHead x == ">") q

seekFields :: String -> [String] -> [[String]]
seekFields name (q:qs)
	| qs == [] = []
	| name `isInfixOf` q = (seqOrStruc qs []):seekFields name qs
	| otherwise = seekFields name qs
seekFields _ [] = error "Parse error in finding fields in .tem file"

seekField :: String -> [String] -> [String]
seekField name (q:qs)
	| qs == [] = []
	| name `isInfixOf` q = (seqOrStruc qs [])
	| otherwise        = seekField name qs
seekField _ [] = error "Parse error in finding field in .tem file"

seqOrStruc :: [String] -> [String] -> [String]
seqOrStruc (q:qs) sofar
	| qs == [] = sofar ++ [q]
	| take 1 q == ">" = sofar
	| otherwise     = seqOrStruc qs (sofar ++ [q])
seqOrStruc [] _ = error "Parse error in finding sequence info in .tem file"
-- \\\ End of such functions /// --
