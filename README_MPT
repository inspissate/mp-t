README for MP-T 31 Oct 12

If you have a question that is not answered in this documentation, or encounter an unexpected error, please write to hill@stats.ox.ac.uk

0. Introduction
1. Download and compilation
2. Making an alignment
3. Tips and Troubleshooting

__________________________________
Introduction

MP-T is a sequence-to-structure alignment program for membrane proteins. MP-T aims to reduce errors in membrane protein models by producing accurate alignments that can be used in modelling programs such as MODELLER and MEDELLER.
__________________________________
Download and compilation

MP-T is written in Haskell, and must be compiled before use. A compiler for Linux, Mac and Windows can be found at http://hackage.haskell.org/platform/ Installation should be a simple click-and-play affair.

After obtaining a compiler, change to the directory containing the MP-T source code files (these have .hs extensions), and type:

ghc --make -O2 -fforce-recomp Aligner.hs -o MPT

This will create an executable, which is run as described in the next section.

__________________________________
Making an alignment

MP-T requires four input files to make an alignment.
	i) A substitution matrix file. This contains data on the relative frequency of substitutions in different parts of a membrane protein. The file comes with this README.
	ii) A .tem file. This contains a sequence that is annotated with predictions of secondary structure, accessible surface area, and membrane positioning. A minimal example (example/structure.tem) is provided with this README. An easy way to make a .tem file is to use the iMembrane webserver at http://opig.stats.ox.ac.uk/
	iii) A fasta file containing the target sequence 
	iv)  A fasta file containing a set of homologous sequences to aid alignment (see Tips section for guidelines on homolog selection). An example is provide in example/homologs.fasta

You should be able to make an alignment by typing:

bin/MPT -s example/structure.tem -q example/query.fasta -a example/homologs.fasta -n 50 -t memMat.dat -o blah.fasta
__________________________________
Tips and Troubleshooting

Q. How many homologous sequences should I supply to MP-T? How do I get them?
A. We found ~125 homologs to work best. These should be a mixture of sequences that are homologous to the target and sequences that are homologous to the template. We searched for homologs to each using the following blast command line options: 

psiblast -evalue 1e-3 -inclusion_ethresh 1e-5 -num_iterations 5

We filtered and combined the homologs subject to the following guidelines:

	i)  No homolog should have <15% identity to its query, or be >3/2 or <2/3 the length of its query
	ii) No two homologs should have >80% sequence identity to each other
	iii)No homolog should be >3/2 or <2/3 the length of the template sequence
	iv) Homologs that satisfy the above criteria should be selected at random (with no regard to BLAST score), and so as to ensure no large numerical bias to sequences homologous to the target or template.  

The performance of MP-T depends strongly on the procedure for selecting homologs: garbage in, garbage out!

Q. Some of the sequences in my input fasta file don't appear in my final alignment. Why?
A. MP-T is not designed to be used as a multiple sequence alignment program. Instead MP-T is designed to make an accurate alignment between the annotated sequence in the .tem file, and the *first* sequence in the fasta file. In making this alignment, MP-T may decide not to use some of the homologs in the fasta file. 

Q. MP-T outputs too many sequences. I only care about a pairwise alignment!
A. You can get a pairwise alignment by copying the first two sequences from the output fasta file. MP-T shows the full alignment to give you an idea about how it arrived at its result.

Q. I get nonsensical results when I use thousands of sequences!
A. MP-T cannot handle more than 998 sequences. On our benchmarks alignment quality dips when ~200 sequences are used.

Q. How do I make an alignment with multiple templates?
A. An example command line would be ./MPT -s "example.tem example2.tem example3.tem" -q query.fasta -a example.fasta -t memMat.dat.
