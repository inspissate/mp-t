2014-07
Data.Hashtable was removed in GHC 7.8. The hashString function from that package was used to randomize the order in which homologs are selected. Thiat function and its dependents are now included as a separate file in accordance with their BSD license.
