{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}	
module Output where
import Profiles (Tree(..),Sequence(..),unscrambleAlignment, flattenTreeWithF)
import Data.List (union,sort)
import qualified Data.Map as Map
import Data.Array.Unboxed (elems)

-- 13/02/2013 Terminal gaps are often 'unaesthetic'. This function makes the alignment prettier by moving single unaligned terminal residues against the rest of the sequence. This function represents a deviation from the version in the MP-T paper.
fix :: String -> String
fix y = iniGap ++ finiGap ++ [lett] ++ restOfseq
	where (iniGap, lett:remains) = span (=='-') y
	      (finiGap, restOfseq) = span (=='-') remains 

-- Calculate summary statistic: percentage of template that is annotated
fracAnnotation :: Sequence -> Double
fracAnnotation z = 100 - 100 * (foldr (\x y -> if x==1 then y+1 else y ) 0 templateAnnotation) / fromIntegral (length templateAnnotation)
	where templateAnnotation = elems $ keyToSeq z
	
process :: [Int] -> String -> String
process (e:es) sequs
 | e >= 0    = take e sequs ++ process es (drop e sequs)
 | otherwise = replicate (-1*e) '-' ++ process es sequs
process _ []  = []
process _ sequs = error sequs

-- Calculate summary statistic: percentage identity... this is averaged over aligned length
calculateID :: [String] -> Double
calculateID x = 100*y/z
   where (y,z) = foldr identicalFx (0,0) $ zip (x!!0) (x!!1) 

identicalFx :: (Char,Char) -> (Double,Double) -> (Double,Double)
identicalFx (res1,res2) (acc,tot)
	| (res1 == res2) && (res1 /= '-') = (acc+1,tot+1) -- Match of ungapped columns
	| (res1 == '-')  || (res2 == '-') = (acc, tot)    -- One column is gapped
	| otherwise                       = (acc,tot+1)   -- Mismatched columns

-- If you introduce an unstable version of printSeqAlignment, change (sort idxSeqs) to idxSeqs.
cutDownAlign :: Int -> [Int] -> [String] -> String
cutDownAlign numStruc idxSeqs alignment =  unlines $ zipWith (\x y -> x ++ "\n" ++ y) outputNames outputSeqs
    where outputSeqs = subalign $ map prettify $ subalign [alignment!!(2*i+1) | i<-idxGood]
	  outputNames = [alignment!!(2*i) | i<-idxGood]
	  subalign y = map (\x -> [x!!(k-1) | k<-(allOccupied y)]) y
	  prettify = fix . reverse . fix . reverse --Set to id to get lone terminal residues back
	  -- one-indexed list of occupied columns in a sequence
	  occupiedIndices = map fst . filter (\(_,z) -> z /= '-') . zip [1..]
	  -- list of indices where at least one sequence has a residue
          allOccupied     = sort . foldr (union.occupiedIndices) [] 
	  -- the indices of target and template sequences in the MSA
          idxGood  = map snd $ filter (\(x,_) -> x <= numStruc + 1) $ zip (sort idxSeqs) [0..]

-- Prints the alignment. isStable could be set to False in a future release (in that case similar sequences would tend to be neighbours).
printSeqAlignment ::  Bool -> [(Int, String)] -> [Int] -> Tree ([Int], [Int])-> Map.Map Int [Char]-> String
printSeqAlignment isStable sequences seqO t names = unlines answer
	where seed = sum $ map abs a1 -- Total alignment length
              Node (a1,_) _ _  = t
	      name  i = idxer i names
	      sequs i = idxer i seqMap
	      estr  i = idxer i estrings
	      seqMap  = Map.fromList sequences
	      estrings = Map.fromList $ zip seqO (flattenTreeWithF (unscrambleAlignment []) [seed] t)
	      answer = case isStable of
		       False -> [name i ++ "\n" ++ process (estr i) (sequs i) | i<-seqO]
		       True  -> [name i ++ "\n" ++ process (estr i) (sequs i) | i<-(sort seqO)]

idxer :: (Ord k) => k -> Map.Map k a -> a
idxer i m = case Map.lookup i m of
                Just x -> x
                Nothing -> error "Sequences have been lost"
