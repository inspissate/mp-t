{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}	
{- Produce a T-COFFEE esque alignment -}
module LibraryExtension (purgeLibrary,combineLibEntry,whatScore,generateLibrary,Mappy) where
import qualified Data.IntMap as Map
{- We run pairwise alignments in the standard all-against-all way. As each alignment comes out of the sausage machine we hurry it away into the library.

19/09/12 The library is a data structure where each element is indexed by a 7 digit code AAA BBBB that specifies a sequence and residue. Each element contains a list of the codes of other residues that are aligned to the first through a pairwise alignment, along with the sequence identity between the pair of sequences in this alignment. For example, an element might contain:

    Seq 15, Res 102, 30% id   	     Seq 16, Res 102, 26% id
	[(0150102, [0.3])	,     (0160102, [0.26])... ]

This is the `primary library'. T-Coffee also does `library extension'. This means that we search for alignments through a 3rd seq. In our case we look up the contacts of both positions and take the union. We do library extension on the fly: we only search for alignments through a 3rd seq when we need them to calculate the score between two columns of the multiple sequence alignment.

03/12/11 WARNING TO THE FUTURE -- In making the run-time compatible with user life-expectancy, I've turned the libraries into additive profiles. However, the code is *very* hard to change sucessfully because of all the invariants it requires to be preserved and the complicated data structures.

19/09/12 The 'additive profiles' mean that after a single alignment has been made, the library is updated (by the teaLady function in Align.hs). A single sequence and residue code is chosen to encapsulate all the information in a column of the profile. Thus we may have the same element as above now containing:

     Seq 15, Res 102 is now also the code for several other residues 
	[(0150102, [0.3,0.29,0.41])	,     (0160102, [0.26])... ]

In practice things may not be exactly as described here -- residue codes and sequence identity lists are maintained in a certain order to speed update.
-}

type Mappy = Map.IntMap [(Int,[Double])]
type LibEntry = [(Int,[Double])]

-- Purge the library to exclude information from sequences that are not part
-- of the final multiple alignment
purgeLibrary :: Mappy -> [Int] -> Mappy
purgeLibrary library seqList = Map.fromList flatLibrary
	where flatLibrary = map (\(k,t) -> (k,filter test t)) $ filter test $ Map.assocs library
	      test (x,_) = (x `quot` 10000) `elem` seqList
{-
OK I've got [ (Key, [(Int,[Double])] ) ]
I want to attack each key, so map requires a function for (Key, [(Int,[Double])])
I want to filter out all the (Int,[Double]) pairs that have a bad Int
So pattern match on the (Key, thing) and map over the thing
The function for that map needs to take a (Int,[Double]) and return either [] or (Int,[Double])
-}

-- Generate the primary library... we'll do library extension on the fly
-- i always increases for a given j, and vice versa, so this procedure builds a list
-- of cood pairs from largest to lowest. No pair occurs twice
generateLibrary :: Mappy -> Double -> (Int, Int) -> Int -> Int -> [Int] -> [Int]
                     -> [(Bool, Bool)] -> Mappy
generateLibrary library pid (i,j) res1 res2 (s1:ss1) (s2:ss2) (x:xs)
    | x == (True,True) = generateLibrary (update library) pid (i,j) (res1+1) (res2+1) ss1 ss2 xs
    | x == (True,False)= generateLibrary library  pid (i,j) (res1+1)  res2    ss1 (s2:ss2) xs
    | x == (False,True)= generateLibrary library  pid (i,j) res1     (res2+1) (s1:ss1) ss2 xs
    | otherwise        = error "There is a problem in the pairwise alignment stage (gap aligned to gap)"
   where update = secondUpdate . firstUpdate
         firstUpdate    = Map.insertWith (++) (who i res1) [((who j res2),[pid])] 
         secondUpdate   = Map.insertWith (++) (who j res2) [((who i res1),[pid])]
generateLibrary library _ _ _ _ _ _ _ = library

-- Need a function that returns the total score for a match between two profiles
-- This is the total score + the implicit score for matching the named pos iden1 and iden2 (if applicable)
whatScore :: Double -> Mappy -> Int -> Int -> Double
whatScore ws library iden1 iden2 =
	case Map.lookup iden1 library of
		Nothing -> 0
		Just x  -> case Map.lookup iden2 library of
				Nothing -> 0
				Just y  -> (combine 0 x y + cosmicNum*(otherNum+thirdNum))/(ws) 
				    where 
					  otherNum = if null q1 then 0 else sum.snd.head $ q1 
					  thirdNum = if null q2 then 0 else sum.snd.head $ q2 
                                          q1 = takeWhile (\(a,_)-> a==iden1).dropWhile (\(a,_)->a>iden1) $ y
                                          q2 = takeWhile (\(a,_)-> a==iden2).dropWhile (\(a,_)->a>iden2) $ x

cosmicNum :: Double
cosmicNum = 0.5 -- 0.5 weights the pairwise and 3-way alignments equally.
		-- numbers > 0.5 favour the pairwise, < 0.5 favour 3-way.

-- iden1 has the lower identifier, so I only care about what happens when iden2 is occupied
combineLibEntry :: Mappy -> Int -> Int -> Mappy
combineLibEntry library iden1 iden2 = 
	case (Map.lookup iden1 library, Map.lookup iden2 library) of
     	  (Just x, Just y) -> Map.insert iden1 (amalgamate [] x y iden2) (Map.delete iden2 library)
	  (Nothing,Just y) -> Map.insert iden1 y (Map.delete iden2 library)
	  _                -> library

-- Merges two profiles of contacting pairs into one. One of the profiles loses its
-- identity, and we must insert this explicitly into the new profile
amalgamate :: LibEntry -> LibEntry -> LibEntry -> Int -> LibEntry
amalgamate acc (x:xs) (y:ys) i2
  | (fst x > fst y) && (fst x /= i2)  = amalgamate                          (x:acc)   xs  (y:ys) i2
  | fst x > fst y                     = amalgamate ((fst x, cosmicNum:(snd x)):acc)   xs  (y:ys) i2
  | (fst x < fst y) && (fst y /= i2)  = amalgamate                          (y:acc) (x:xs)  ys   i2
  | fst x < fst y                     = amalgamate ((fst y, cosmicNum:(snd y)):acc) (x:xs)  ys   i2
  | fst x /= i2                       = amalgamate ((fst x, merge [] (snd x) (snd y)):acc) xs ys i2
  | otherwise = amalgamate ((fst x, cosmicNum:(merge [] (snd x) (snd y))):acc) xs ys i2
amalgamate acc (x:xs) [] i2
  | fst x == i2                       = amalgamate ((fst x, cosmicNum:(snd x)):acc) xs [] i2
  | otherwise                         = amalgamate (x:acc) xs [] i2
amalgamate acc [] (y:ys) i2
  | fst y == i2                       = amalgamate ((fst y, cosmicNum:(snd y)):acc) [] ys i2
  | otherwise                         = amalgamate (y:acc) [] ys i2
amalgamate acc [] [] _  = reverse acc

-- Put the cosmicNum or else the smallest first
merge :: [Double] -> [Double] -> [Double] -> [Double]
merge acc (x:xs) (y:ys)
  | x == cosmicNum = merge (x:acc) xs (y:ys)
  | y == cosmicNum = merge (y:acc) (x:xs) ys
  | x < y = merge (x:acc) xs (y:ys)
  | x > y = merge (y:acc) (x:xs) ys
  | otherwise = merge (x:y:acc) xs ys
merge acc [] ys = (reverse acc) ++ ys
merge acc xs [] = (reverse acc) ++ xs 

-- Return the 7 digit index for a sequence number and residue id 
who :: Int -> Int -> Int
who seq_id res_id = seq_id*(10000) + res_id

combine :: Double -> [(Int,[Double])] -> [(Int,[Double])] -> Double
-- The lists (x:xs) and (y:ys) run from higher ids to lower ones.
combine acc (x:xs) (y:ys)  
   | fst x > fst y = combine  acc xs (y:ys)  
   | fst x < fst y = combine  acc (x:xs) ys  
   | otherwise     = combine (acc + scoreMe (snd x) (snd y)) xs ys
combine acc _ _ = acc

scoreMe :: [Double] -> [Double] -> Double
scoreMe x y
   | head x == cosmicNum = (sum y)*cosmicNum + scoring 0 (a-1)   b   (drop 1 x)  y
   | head y == cosmicNum = (sum x)*cosmicNum + scoring 0  a   (b-1)       x  (drop 1 y)
   | otherwise = scoring 0 a b x y
  where a = fromIntegral.length $ x
        b = fromIntegral.length $ y


-- The lists (x:xs) and (y:ys) run from lower ids to higher ones.
scoring :: Double -> Double -> Double -> [Double] -> [Double] -> Double
scoring acc a b (x:xs) (y:ys)
  | x < y         = scoring (acc + b*x)   (a-1)  b xs (y:ys)
  | otherwise     = scoring (acc + a*y) a    (b-1) (x:xs) ys
scoring acc _ _ _ _ = acc
