{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}		
import Profiles (Profile(..),Sequence(..), newStrucProf,newStrucSeq,newSeq,newProf,tableIndex2GapIndex)
import Tables (helpUnusualChars,getQuery,createAeij,extractSequ,getStruc)
import Trees
import Output
import System.Environment (getArgs)
import Data.Array.IO
import Data.Array.Unboxed (UArray)
import Data.List (nubBy)
import qualified Data.Map as Map
import System.Console.GetOpt
import qualified Data.IntMap as IntMap

data Options = Options {
    inpStruc    :: FilePath, -- File of structure
    inpQuery    :: FilePath, -- File of query sequence
    inpHomologs :: FilePath, -- File of homologous sequences
    inpTables   :: FilePath, -- File of substitution table data
    outModel    :: FilePath, -- File to output strucs + target alignment
    outFull     :: FilePath, -- File to output full sequence alignment
    numSeqs     :: Int       -- Maximum number of sequences to use
   }

defaultOptions :: Options
defaultOptions = Options {
    inpStruc  = [],
    inpQuery = [],
    inpHomologs = [],
    inpTables = [], --"/data/pegasus/hill/multipleAlignmentTables090212Mafft.dat"
    outModel = [],
    outFull = [],
    numSeqs = 125
   }

options :: [OptDescr (Options -> Options)]
options = [Option "s" ["struc"]  (ReqArg (\arg opt -> opt {inpStruc = arg :: String}) "FILE") "Input .tem file",
           Option "q" ["query"] (ReqArg (\arg opt -> opt {inpQuery = arg}) "FILE") "Input fasta formatted query sequence",
           Option "a" ["homologs"] (ReqArg (\arg opt -> opt {inpHomologs = arg}) "FILE") "Input fasta formatted homologous sequences",
	   Option "t" ["tables"] (ReqArg (\arg opt -> opt {inpTables = arg :: String}) "FILE") "Substitution tables file",
           Option "o" ["out_modelling"] (ReqArg (\arg opt -> opt {outModel = arg :: String}) "FILE") "File to which alignment of template structures with sequence will be written",
	   Option "f" ["out_full"] (ReqArg (\arg opt -> opt {outFull = arg :: String}) "FILE") "File to which full multiple sequence alignment will be written",
	   Option "n" ["num_seqs"] (ReqArg (\arg opt -> opt {numSeqs = read arg :: Int}) "Int") "Maximum number of sequences to use from input alignment file"]

header :: String
header = "\n   Example usage: MPT -s \"ANNOTATED_1.tem ANNOTATED_2.tem\" -q QUERY.fasta -a HOMOLOGS.fasta -t memMat.dat -o OUTFILE\n\n\n"

help :: String
help = "MPT version 1.2.1\n\nHELP: MP-T is a sequence-to-structure alignment program for membrane proteins. Structural information is incorporated into the alignment via sequence annotations in the form of a .tem file. Sequence information must be provided as a fasta formatted file of homologs. Although MP-T produces a multiple sequence alignment as output, it is only designed to ensure a good alignment between the structure and the query sequence. In most cases MP-T's alignment will ignore some of the homologs. \n\n"

readLines :: FilePath -> IO [String]
readLines x = fmap lines $ readFile x

makeNonRedundant :: [(String, String)] -> [(String, String)]
makeNonRedundant = nubBy (\x y -> fst x == fst y)

main :: IO ()
main = do
        args     <- getArgs
        let (actions, nonOpts, msgs) = getOpt Permute options args
	case (actions, nonOpts, msgs) of
            ([],[],[])   -> error $ help ++ usageInfo header options
            (_ , [] ,[]) -> return ()
            (_ ,[],errs) -> error $ concat errs ++ usageInfo header options
            (_ ,nons, _) -> error $ "Unrecognised arguments: " ++ (unwords nons)
        let opts = foldl (flip id) defaultOptions actions
	if null (inpStruc opts) then error $ "Use the -s flag to supply an annotated .tem file" else return ()
	if null (inpQuery opts) then error $ "Use the -q flag to supply a query sequence" else return ()
	if null (inpHomologs opts) then error $ "Use the -a flag to supply a set of sequences in FASTA format" else return ()
	if null (inpTables opts) then error $ "Use the -t flag to supply the location of the substitution tables" else return ()
	if null (outModel opts) then error $ "Use the -o flag to specify an output file" else return ()
	tems <- mapM readLines $ filter (not.null) $ words (inpStruc opts)
        [tables, query, homologs] <- mapM readLines [inpTables opts, inpQuery opts, inpHomologs opts]

-- ** Deal with the scoring system: tables, annotations and gap penalties ** --
	let -- iMembrane has both a "solvent accessibility" and "accessibility" field. We need the first.
	    envs = ["membrane contact","membrane layer","secondary", "solvent"]
	    -- Gap pens below are for MAFFT all-to-all at 60% ID
	    (totGapsS:inputtedS) = [[-200,-10],[-300,-20],[-300,-20],[-300,-20],[-300,-20],[-200,-10],[-200,-10],[-200,-10],[-200,-10],[-360,-60],[-360,-60],[-220,-20],[-220,-20]]	
	    (logodds,envList,aa2id) = createAeij tables
	    key2gapPair = tableIndex2GapIndex envList (totGapsS:inputtedS)

-- ** Deal with the current input: .tem files and .fasta file ** --
	let
	    target = getQuery query
	    structures@(template:allStrucs) = map getStruc tems
	    (badSeqs,allHomologs) = extractSequ homologs
	if fst target == fst template then error "Target and template are identical" else return ()
	if length structures /= length (makeNonRedundant structures) then error "Please use non-redundant structure files" else return ()
	if not (null badSeqs) then putStr $ "The following homologous sequences contain non-standard amino acids: " ++ badSeqs ++ helpUnusualChars else return()
	putStrLn $ "Aligning target sequence " ++ snd target ++ " with template(s) " ++ unwords (map snd (template:allStrucs))
	let filteredSeqs = take (numSeqs opts) $ makeNonRedundant $ template:target:allStrucs ++ allHomologs
	    names = Map.fromList $ zip [1..] (map snd filteredSeqs)
	    sequences = zip [1..] (map fst filteredSeqs)
	    longestSeq = maximum $ map (length.fst) filteredSeqs

	    l = length sequences
	    m = length structures

	-- Make a set of sequence objects, remembering to treat the structures as special
 	profArray <- newListArray (1,l) $! [ if i `elem` [0..m] && i/=1 then newStrucSeq (sequences!!i) aa2id (tems!!(if i==0 then i else i-1)) envs envList key2gapPair else newSeq (sequences!!i) aa2id totGapsS | i<-[0..l-1]] :: IO (IOArray Int Sequence)

	-- Extract the template structure, so that its annotated fraction can be reported
	templateProfile <- readArray profArray 1

        -- Build the library of pairwise alignments and estimate evolutionary distance between each pair from seq id
	(distMatrix, library) <- getOrder grishin longestSeq l m profArray logodds key2gapPair
	profileArray <- newListArray (1,l) $! [ if i `elem` [0..m] && i/=1 then newStrucProf (sequences!!i) (tems!!(if i==0 then i else i-1)) envs envList else newProf (sequences!!i) | i<-[0..l-1]] :: IO (IOArray Int Profile)
	finalProf <- performProgAlign distMatrix [1..l] profileArray library
        let finalAlignment   = printSeqAlignment True sequences (seqOrdering finalProf) (seqs finalProf) names
	    cutDownAlignment = cutDownAlign m (seqOrdering finalProf) (lines finalAlignment)
	    cutDownFurther   = lines cutDownAlignment -- In this case, lines.unlines /= id
        if (not.null) (outFull opts) then writeFile (outFull opts) finalAlignment else return ()        
	writeFile (outModel opts) $ cutDownAlignment
	putStr $ concat ["Number of sequences: ", show(length(seqOrdering finalProf)), "\nPercentage Identity: ", show (round $ calculateID [cutDownFurther!!i | i<-[1,3]] :: Int), "\nPercentage of template that is annotated: ", show (round $ fracAnnotation templateProfile :: Int),"\n"]

getOrder :: (Double -> Double) -> Int -> Int -> Int -> IOArray Int Sequence -> UArray (Int, Int, Int) Int -> Map.Map Int [Int] -> IO (IOUArray (Int, Int) Double,IntMap.IntMap [(Int,[Double])])
getOrder method longestSeq l m profArray logodds key2gap = do
	(similMat,library) <- makeDistMat longestSeq m profArray logodds l key2gap
	distMatrix <- mapArray method similMat
	return (distMatrix, library)
