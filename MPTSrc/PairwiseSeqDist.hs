{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}	
module PairwiseSeqDist (needwuSeq) where
import Data.Array.IO
import Data.Array.Unboxed
import Data.Array.Base (unsafeWrite, unsafeRead)
import Align (endPoint)

type Gaps = UArray (Int,Int) Int
type Pos  = (Int,Int,Int)

{- If recycling the array (see note to altschul in Profiles.hs, would have to pass bounds in directly -}
needwuSeq :: Int -> Int -> Gaps -> IOUArray Pos Int -> IO [(Bool,Bool)]
needwuSeq m n gaps1 arr = do
    (_,(_,offset,_)) <- getBounds arr
    let idxer (i,j,k) = 3*(offset+1)*i + 3*j + k + 1
    unsafeWrite arr 1 0 -- Set (0,0,0) element to (0,1)
    mapM_ (\(x1,x2) -> unsafeWrite arr x1 x2)
       ([((idxer (0,j,0)),-32768) | j<-[1..n]] ++
        [((idxer (i,0,0)),-32768) | i<-[1..m]] ++
        [((idxer (0,j,-1)),-32768) | j<-[0..n]] ++
        [((idxer (i,0,-1)),-50*i) | i<-[1..m]] ++ --Set gap pen
        [((idxer (0,j,1)),-50*j) | j<-[1..n]] ++  --Set gap pen
        [((idxer (i,0,1)),-32768) | i<-[0..m]])
    sequence_ [updateArrayStart arr (gaps1!(i,1)) (gaps1!(i,2)) i n offset | i<-[1..m]]
    p <- endPoint arr m n 50 $ [(a,n,c)| a<-[1..m], c<-[-1,0,1]]++[(m,b,c) | b<-[1..n], c<-[-1,0,1]]
	-- Above set gap pen
    acc <- alignAssist [] arr p gaps1
    return $! acc ++ overhang m n p

updateArrayStart :: IOUArray Pos Int -> Int -> Int ->  Int -> Int -> Int -> IO ()
updateArrayStart arr dop e i n offset = do
    u1 <- readArray arr (i-1,0,0)
    d2 <- readArray arr (i-1,0,-1)
    updateArray arr u1 d2 dop e (i,1) n offset

overhang :: Int -> Int -> Pos -> [(Bool,Bool)]
overhang m n (i,j,_) = lotsOf (n-j) (False,True) ++ lotsOf (m-i) (True,False)
	where lotsOf x = (take x).repeat

alignAssist :: [(Bool,Bool)] -> IOUArray Pos Int -> (Int,Int,Int) ->
 UArray (Int,Int) Int  -> IO [(Bool,Bool)]
alignAssist acc arr (i,j,k) gaps1
	| i == 0 = return (take j (repeat (False,True)) ++ acc)
	| j == 0 = return (take i (repeat (True,False)) ++ acc)
	| k == 0 = do
		p1 <- readArray arr (i-1,j-1,-1)
		p2 <- readArray arr (i-1,j-1, 0)
		p3 <- readArray arr (i-1,j-1, 1)
		case (p2 >= p1) && (p2 >= p3) of
		  True -> alignAssist ((True,True):acc) arr (i-1,j-1,0) gaps1
		  False -> case (p3 >= p1) of
			True -> alignAssist ((True,True):acc) arr (i-1,j-1,  1) gaps1
			False -> alignAssist ((True,True):acc) arr (i-1,j-1,-1) gaps1
	| k == -1 = do
		p1 <- readArray arr (i-1,j,-1)
		p2 <- readArray arr (i-1,j, 0)
		case (p2 + gaps1!(i,1) >= p1 + gaps1!(i,2)) of -- *
		  True -> alignAssist ((True,False):acc) arr (i-1,j,0) gaps1
		  False ->alignAssist ((True,False):acc) arr (i-1,j,-1) gaps1
			
	| k == 1 = do
		p2 <- readArray arr (i,j-1, 0)
		p3 <- readArray arr (i,j-1, 1)
		case (p3 + gaps1!(i,2) >= p2 + gaps1!(i,1)) of -- *
		  True -> alignAssist ((False,True):acc) arr (i,j-1, 1) gaps1
		  False-> alignAssist ((False,True):acc) arr (i,j-1,0) gaps1
alignAssist _ _ (_,_,_) _ = error "Problem during traceback of alignment"

updateArray :: IOUArray Pos Int ->
                    Int -> Int -> Int -> Int -> (Int,Int) -> Int -> Int -> IO ()
updateArray arr u1 d2 dop e (i,j) n offset = do
    let idxer (f1,f2,f3) = 3*(offset+1)*f1 + 3*f2 + f3 +1 :: Int
        idx = idxer (i,j,-1)
    d1 <- unsafeRead arr (idxer (i-1,j,-1))
    s1 <- unsafeRead arr (idxer (i-1,j,0))
    unsafeWrite arr idx (max (dop+s1) (e+d1))
    i1 <- unsafeRead arr (idxer (i,j-1,1))
    t1 <- unsafeRead arr (idxer (i,j-1,0))
    unsafeWrite arr (idx+2) (max (dop+t1) (e+i1))
    i2 <- unsafeRead arr (idxer (i-1,j-1,1))
    sub <- unsafeRead arr (idxer (i,j,0))
    unsafeWrite arr (idx+1) (sub+(max u1 (max i2 d2)))
    case j<n of
        True -> updateArray arr s1 d1 dop e (i,j+1) n offset
        False -> do return ()
