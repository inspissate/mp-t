{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}		
module Profiles (Tree(..),Profile(..),newStrucProf,newStrucSeq,newProf,newSeq,henikoff,altschul,Sequence(..),unscrambleAlignment, flattenTreeWithF,idx, tableIndex2GapIndex, makeSeq, createGaps, LogOdds) where
import Data.Array.Unboxed
import Data.Array.IO
import qualified Data.Map as Map (lookup, fromList, keys, Map)
import Tables (makeProfile, readEnv)
import Data.Array.Base (unsafeWrite)
import LibraryExtension (whatScore,Mappy)

-- Represent alignments as pairs of e-strings (see Edgar 2004)
data Tree a = Leaf | Node !a (Tree a) (Tree a) deriving (Show,Read)

data Profile = Profile {
    keyToAll :: UArray Int Int, -- List of all environments at each position
    matches :: UArray Int Int,       -- 7 digit ID in ***#### seq/resi form of amino-acid in a profile
    normish:: UArray Int Double, -- Total number of AAs, Gap opens and Gap exts in each column
    seqs :: Tree ([Int],[Int]),
    seqOrdering :: ![Int] -- List of identifiers of sequences in profile
    }

data Sequence = Sequence {
    keyToSeq :: UArray Int Int,
    gapToSeq :: UArray (Int,Int) Int,
    pssmSeq :: UArray Int Int
    }

type LogOdds = UArray (Int,Int,Int) Int

-- Combine two e-strings: the alignment is changed by string t, and then has string s applied
-- It's ugly, and inserts '0's where none are needed... ...but it works.
unscrambleAlignment :: [Int] -> [Int] -> [Int] -> [Int]
unscrambleAlignment acc s [] = (reverse acc) ++ s
unscrambleAlignment acc [] t = (reverse acc) ++ t
unscrambleAlignment acc (s1:ss) (t1:ts)
 | s1 < 0        = unscrambleAlignment (s1:acc) ss (t1:ts) -- Gaps in later alignment win
 | s1 == 0       = unscrambleAlignment acc ss (t1:ts)      -- If either e-string contains a
 | t1 == 0       = unscrambleAlignment acc (s1:ss) ts      -- 0 ignore it
 | s1 >= (abs t1)= unscrambleAlignment (t1:acc) (s1-(abs t1):ss) ts -- First string accounts for 2nd
 -- At this stage we know s1 > 0 and abs(t1) > s1. We don't know the sign of t1
 | t1 < 0        = unscrambleAlignment (-1*s1:acc) ss (t1 + s1:ts)
 | t1 > 0        = unscrambleAlignment (s1:acc) ss (t1 - s1 :ts)
unscrambleAlignment a b c = error $ "Terrible problems with the edit-strings. Panic! " ++ (show a) ++ (show b) ++ show (c)

-- Need a function to march down a tree, unscrambling as it goes.
-- A more efficient version would be implemented as at:
-- http://www.csse.monash.edu.au/~lloyd/tildeFP/Haskell/1998/Tree/
flattenTreeWithF :: (a -> a -> a) -> a -> Tree (a,a) -> [a]
flattenTreeWithF _ c Leaf = [c]
flattenTreeWithF g c (Node (a1,a2) l r) =
     flattenTreeWithF g (g c a1) l ++ flattenTreeWithF g (g c a2) r

-- Turn a string into an array
makeSeq :: [Int] -> UArray Int Int
makeSeq x = listArray (1,length x) x

-- I need a function that makes a profile from a structure.
newStrucProf :: (Int, String)
                     ->  [String]
                     -> [String]
                     -> Map.Map String Int
                           -> Profile
newStrucProf (seqNum,sequ) tem envs envList =
    Profile keyish matchish norms Leaf [seqNum]
        where lotsOf = (take (length sequ)).repeat
              n = length sequ
	      keyIntermediate = keys tem envs envList
	      keyish = makeSeq keyIntermediate
	      norms = listArray (1,n) (lotsOf 1)
	      matchish = makeSeq [seqNum*(10000) + i | i <-[1..n]]

-- and a function that makes a profile from a sequence
newProf :: (Int,String) -> Profile
newProf (seqNum,sequ) = Profile keyish matchish norms Leaf [seqNum]
        where lotsOf = (take (length sequ)).repeat
              n = length sequ
              keyish = listArray (1,n) (lotsOf 1) :: UArray Int Int
	      norms = listArray (1,n) (lotsOf 1)
	      matchish = makeSeq [seqNum*(10000) + i | i <-[1..n]]

-- I need a function that makes a `Sequence' from a structure.
newStrucSeq :: (Int, String)
                     -> Map.Map Char Int
                     -> [String]
                     -> [String]
                     -> Map.Map String Int
                     -> Map.Map Int [Int]
                    	-> Sequence
newStrucSeq (_,sequ) aa2id tem envs envList key2gap
	| l /= m = error $ "sequence length is " ++ show l ++ " but environment lengths are " ++ show m
 	| l /= n = error $ "sequence length is " ++ show l ++ " but gap environment lengths are " ++ show n
	| otherwise = Sequence keyish gapish pssmish
      where pssmish = makeSeq [idx i aa2id | i<-sequ]
	    keyIntermediate = keys tem envs envList
	    keyish  = makeSeq keyIntermediate
	    gapish  = createGaps keyIntermediate key2gap
	    m = snd.bounds $ keyish
	    n = fst.snd.bounds $ gapish
	    l = length sequ

-- and a function that makes a `Sequence' from a sequence
newSeq :: (Int,String) -> Map.Map Char Int -> [Int] -> Sequence
newSeq (_,sequ) aa2id gapVal= gapish `seq` pssmish `seq` keyish `seq` Sequence keyish gapish pssmish
        where lotsOf = (take n).repeat
              n = length sequ
              keyish = makeSeq (lotsOf 1)
              pssmish = makeSeq [idx i aa2id | i<-sequ]
              gapish = listArray ((1,1),(n,2)) (concat (lotsOf gapVal))

-- Get a list of the gap penalties for each position
createGaps :: [Int] -> Map.Map Int [Int] -> UArray (Int,Int) Int
createGaps key key2gap = listArray ((1,1),(length key,2)) (concat [idx k key2gap | k <- key])

tableIndex2GapIndex :: Map.Map String Int -> [[Int]] -> Map.Map Int [Int]
tableIndex2GapIndex envList inputted = key2gap
	where gapEnvs  = Map.fromList $ zip ["HH","HE", "NH", "NE", "HC", "HP", "NC", "NP", "TH", "TE","TC","TP"] (tail inputted)
	      key2gap = Map.fromList $ (1,head inputted):[(idx p envList, idx ((take 2.drop 1) p) gapEnvs)| p<-(Map.keys envList)]
	

-- Get a list of environments at each position. The '?' was introduced on 07/06/12
keys :: [String] -> [String] -> Map.Map String Int -> [Int]
keys tem envs envList = [ if '?' `elem` (tail p) then 1 else idx p envList | p <-b ]
   where b = map (\(_:xs)-> xs) $ filter (\(x:_)->x/='-' && x/='*' && x/='\r')
                        (makeProfile tem (readEnv "structure" tem) envs)


-- When constructing the initial distance matrix, we want to do an alignment as quickly as possible. We call a separate version of Henikoff, which will become swift. We needn't check for the environment because we do the structures first.
altschul :: IOUArray (Int,Int,Int) Int -> Sequence -> Sequence -> LogOdds -> IO (IOUArray (Int,Int,Int) Int)
altschul arr (Sequence keyToAll1 _ seq1) (Sequence _ _ seq2) logodds = do
     (_,(_,offset,_)) <- getBounds arr     
     let  m1 = (snd.bounds) seq1
          m2 = (snd.bounds) seq2
          idxer f1 = 3*(offset+1)*f1 + 4
     sequence_ [altschuler arr seq2 logodds (idxer c) 1 m2 (keyToAll1!c) (seq1!c) | c<-[1..m1]]
     return arr

{- The full expression for indexing is: let idxer f1 f2 = 3*(m2+1)*f1+3*f2+1 -}
altschuler :: IOUArray (Int,Int,Int) Int -> UArray Int Int -> LogOdds -> Int -> Int ->
		Int -> Int -> Int -> IO ()
altschuler arr seq2 logodds k j m2 p q = do
    unsafeWrite arr k (logodds!(p,seq2!j,q))
    case j<m2 of
        True -> altschuler arr seq2 logodds (k+3) (j+1) m2 p q
        False -> return ()


 -- Take the two profiles, and use them to construct the scoring matrix
henikoff :: Double -> Profile ->  Profile -> Mappy -> IO (IOUArray (Int,Int,Int) Int)
henikoff ws prof1 prof2 library = do
    arr <- newArray_ ((0,0,-1),(m1,m2,1)) :: IO (IOUArray (Int,Int,Int) Int)
    sequence_ [track ws prof1 prof2 arr c d (idxer c d) library | c<-[1..m1],d<-[1..m2]]
    return arr
        where m1 = (snd.bounds.keyToAll) prof1
              m2 = (snd.bounds.keyToAll) prof2
	      idxer f1 f2 = 3*(m2+1)*f1 + 3*f2 + 1

track :: Double -> Profile -> Profile -> IOUArray (Int,Int,Int) Int -> Int -> Int -> Int -> Mappy  -> IO ()
track ws (Profile _ match1 norm1 _ _) (Profile _ match2 norm2 _ _ ) arr c d indexed  library =  
   unsafeWrite arr indexed $ round $ 
	(500 * norm * (whatScore ws library (match1!c) (match2!d)))
   where norm = 1/((norm1!c)*(norm2!d))

idx :: (Ord k, Show k, Show a) => k -> Map.Map k a -> a
idx i m = case Map.lookup i m of
                Just x -> x
                Nothing -> error ("Could not find symbol " ++ (show i) ++ " in map " ++ (show m))
