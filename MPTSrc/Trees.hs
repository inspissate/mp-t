{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}	
module Trees (performProgAlign,makeDistMat,grishin) where
import Data.Array.IO
import Profiles (henikoff,altschul,Profile(..),Sequence(..),LogOdds, makeSeq,createGaps)
import Align (alignProfiles)
import PairwiseSeqDist (needwuSeq)
import Data.Array.Unboxed
import qualified Data.Map as Map
import Data.List (nub)
import Control.Monad (foldM)
import LibraryExtension (purgeLibrary,generateLibrary,Mappy)
import qualified Data.IntMap as IntMap

type DistMat = IOUArray (Int,Int) Double

-- Make matrix from pairwise distances
makeDistMat :: Int -> Int -> IOArray Int Sequence -> LogOdds
                    -> Int -> Map.Map Int [Int] -> IO (DistMat,Mappy)
makeDistMat longestSeq strucNum profArray logodds l key2gap = do
    arr <- newArray ((1,1),(l,l)) 1 :: IO (IOUArray (Int,Int) Double)
    alignArr <- newArray_ ((0,0,-1),(longestSeq,longestSeq,1)) :: IO (IOUArray (Int,Int,Int) Int)
    bestPidSoFar <- newArray (1,l) 0 :: IO (IOUArray Int Double)
    putStrLn "Building sequence library" -- Need to feed length of longest seq into this function
    library <- foldM (monsterFunc bestPidSoFar strucNum profArray logodds arr key2gap alignArr) IntMap.empty [(i,j) | i<-[1..l], j<-[i+1..l]]
    return (arr,library)

--This is a terrible function that does lots of I/O
monsterFunc :: IOUArray Int Double -> Int -> IOArray Int Sequence -> LogOdds -> DistMat
		 -> Map.Map Int [Int] -> IOUArray (Int,Int,Int) Int -> Mappy -> (Int,Int) -> IO Mappy
monsterFunc bestPidSoFar strucNum profArray logodds arr key2gap alignArr library (i,j) = do
	-- print (i,j)
	-- Read the two sequences, and calc the scores for all matches between them
	m1 <- readArray profArray i
        m2 <- readArray profArray j
	-- Use environments from the sequence with the most trustworthy annotation
        (p1,p2,k,m) <- assignStrucChoice strucNum i j m1 m2 bestPidSoFar
	let (_,len1)  = bounds (pssmSeq p1)
            (_,len2)  = bounds (pssmSeq p2)
	-- print $ "len1 is " ++ show len1
	-- print $ "len2 is " ++ show len2
	submat <- altschul alignArr p1 p2 logodds
	-- Do the alignment, and return the list of Booleans that tells us the matches
	value <- needwuSeq len1 len2 (gapToSeq p1) submat
	-- Use the list to work out the pid and length of each seq: write these into arrays
	-- for the tree-building algorithm to read later
	let s1  = elems (pssmSeq p1)
            s2  = elems (pssmSeq p2)
	    pid = updateSeq1 0 0 s1 s2 value 
	getDist arr (i,j) pid
	bestPid <- readArray bestPidSoFar j
	-- Only transfer the annotation from the closest-matching structure (if sufficiently similar). Do not overwrite the annotation of a structure with anything else, or transfer annotations from sequences
	transferAnnotation ((pid > bestPid) && (pid > 0.2) && (i<=strucNum+1) && (i/=2) && (j>strucNum))
				pid bestPidSoFar j p1 p2 value profArray key2gap
	-- Update the library based on the observed pairwise matches
        let library' = generateLibrary library pid (k,m) 1 1 s1 s2 value
        return library'

-- Choose the sequence from which to use transferred structural information.
assignStrucChoice :: Int -> Int -> Int -> Sequence -> Sequence -> IOUArray Int Double -> IO (Sequence,Sequence,Int,Int)
assignStrucChoice strucNum i j m1 m2 bestPidSoFar =
   case i <= strucNum of
     True  -> return (m1,m2,i,j)	
     False -> do
	pid1 <- readArray bestPidSoFar i
	pid2 <- readArray bestPidSoFar j
	case pid1>=pid2 of
		True -> return (m1,m2,i,j)
		False-> return (m2,m1,j,i)

updateKeys :: [Int] -> [Int] -> [Int] -> [(Bool,Bool)] -> [Int] 
updateKeys acc (k1:k1s) (k2:k2s) (x:xs)
	| x==(True,True)  = updateKeys (k1:acc)   k1s       k2s   xs
	| x==(True,False) = updateKeys   acc      k1s    (k2:k2s) xs
	| x==(False,True) = updateKeys (k2:acc) (k1:k1s)    k2s   xs
	| otherwise = error "A gap is aligned to a gap in a pairwise alignment"
updateKeys acc _ k2s _ = (reverse acc) ++ k2s

transferAnnotation :: Bool -> Double -> IOUArray Int Double -> Int -> Sequence -> Sequence -> [(Bool,Bool)] -> IOArray Int Sequence -> Map.Map Int [Int] -> IO ()
transferAnnotation True pid bestPidSoFar j p1 p2 value profArray key2gap = do
	let k1 = elems $ keyToSeq p1
	    k2 = elems $ keyToSeq p2
	    newK2 = updateKeys [] k1 k2 value
	    newGs = createGaps newK2 key2gap
	writeArray profArray j (Sequence (makeSeq newK2) newGs (pssmSeq p2))
	writeArray bestPidSoFar j pid
transferAnnotation _ _ _ _ _ _ _ _ _ = return () -- Only do something if p1 is the structure

{- ****************************************** -}

-- Log the fraction of identical residues after it has been calculated
getDist :: DistMat -> (Int,Int) -> Double -> IO ()
getDist arr (i,j) d  = do
    writeArray arr (i,j) d -- NB: this is inefficient, because only 1/2 the matrix is used
    writeArray arr (j,i) d
    
{- ****************************************** -}
-- updateSeq1 takes a list of bools from an alignment, and uses it to
-- calculate the fractional identity that is then used in the Kimura distance.
updateSeq1 :: Double -> Double -> [Int] -> [Int] -> [(Bool,Bool)] -> Double
updateSeq1 acc tot (s1:ss1) (s2:ss2) (x:xs)
    | x == (True,True) && (s1 == s2) = updateSeq1 (acc + 1) (tot + 1) ss1 ss2 xs
    | x == (True,True) = updateSeq1 acc (tot+1) ss1 ss2 xs
    | fst x == True = updateSeq1 acc tot ss1 (s2:ss2) xs
    | otherwise = updateSeq1 acc tot (s1:ss1) ss2 xs
updateSeq1 acc tot _ _ _ = if tot == 0 then 0 else acc/tot

-- Get evolutionary distance using `frac': the fraction of sites that are the same
-- Formula (14) from Grishin 1995 "Estimation of the number of amino acid subst..."
grishin :: Double -> Double
grishin frac
  | frac > 0.95 = (1-frac)
  | frac < 1e-3 = 4559 -- If frac = 0 convergence doesn't occur. This handles the edge case
  | otherwise   = newtonRaphson f f' 0.1
	where f  = \z -> (log(1+2*z))/(2*z) - frac
	      f' = \z -> (1/z)*((1/(1+2*z))-(f z) -frac)

-- Simple iterative solver, converges due to the concavity of grishin fx
newtonRaphson :: (Double -> Double) -> (Double -> Double) -> Double -> Double
newtonRaphson f f' x
	| abs (newX - x) < tol = newX
	| otherwise = newtonRaphson f f' newX
       where newX = x - (f x)/(f' x)
	     tol  = 1e-5

updateArray :: IOArray Int Profile -> Mappy -> 
                (Int,Int) ->  IO (Profile,Mappy,Double)
updateArray arr library (i,j) = do
    p1 <- readArray arr i
    p2 <- readArray arr j
    (_,numOfSeqs) <- getBounds arr
    submat <- henikoff (fromIntegral numOfSeqs) p1 p2 library -- Make new profile taking envs primarily from p1
    (newProfile,newLib) <- alignProfiles p1 p2 submat library -- distMatrix
    let noSeqx = length $ seqOrdering p1 -- (snd.bounds) (seqs p1)
        noSeqy = length $ seqOrdering p2 -- (snd.bounds) (seqs p2)
        weightx = (fromIntegral noSeqx)/(fromIntegral (noSeqx+noSeqy))
    return (newProfile,newLib,weightx)

-- ************* Functions for Tree building **************
{- In the past we never explicitly calculated a tree. This meant that if we had

	A
      /   \
     B     C
   /  \

and if our sequences of interest were all aligned by B, we needlessly did all the
alignments that lead to C. filterTree prevents this needless work. -}

performProgAlign :: DistMat -> [Int] -> IOArray Int Profile -> Mappy -> IO Profile
performProgAlign arr allowed profArray library = do
	paired <- upgma arr allowed []
	let survivors = reverse $ filterTree [1,2] paired
	    uniqueSeq = nub $ concatMap (\(x,y) -> [x]++[y]) survivors
	    library'  = purgeLibrary library uniqueSeq
	beginAlign profArray library' survivors >>= return

filterTree :: [Int] -> [(Int,Int)] -> [(Int,Int)]
filterTree acc ((a,b):xs)
	| (min a b) `elem` acc = (a,b) : filterTree ((max a b):acc) xs
	| otherwise = filterTree acc xs
filterTree _ [] = []

beginAlign :: IOArray Int Profile -> Mappy -> [(Int,Int)] -> IO Profile
beginAlign profArray library ((i,j):sx) = do
	putStrLn ("Combining " ++ show i ++ " and " ++ show j)
        (newProfile,newLib,_) <- updateArray profArray library (i,j) -- Actually update array here
        writeArray profArray i newProfile
	case null sx of
		False -> beginAlign profArray newLib sx
		True  -> return newProfile
beginAlign _ _ [] = error "Unexpected error: target and template are lost during tree building"

-- *************** Functions for UPGMA **************
-- Input takes the form of a matrix of distances
upgma :: DistMat -> [Int] -> [(Int,Int)] -> IO [(Int,Int)]
upgma arr allowed outList = do
            let xs = [(i,j) | i<-allowed, j<-allowed, j>i] -- Read elements in one half diag
            (x,y) <- getMin arr (head xs) (tail xs)	   -- Find first minimum (x,y) where x<y
	    let allowedN = filter (/= y) allowed           -- Exclude y from future consideration
	        fil = \z -> filter (\(a,_) -> a == z) outList
	        dealWith p = if null p then 0 else snd.head $ p
		noSeqx  = 1 + (length $ filterTree [x,dealWith (fil x)] outList)  -- # seqs in the first profile
		noSeqy  = 1 + (length $ filterTree [y,dealWith (fil y)] outList)
		weightx = (fromIntegral noSeqx) / (fromIntegral (noSeqx + noSeqy))
            mapM_ (average arr weightx) [(x,y,b) | b<-allowed, b/=x]
            case (length allowedN == 1) || ((x==1) && (y==2)) of
                  False -> upgma arr allowedN ((x,y):outList)
                  True ->  return ((x,y):outList)

-- Update distances within distMat after merging of profiles
average :: DistMat -> Double -> (Int,Int,Int) -> IO ()
average arr weightx (x,y,b)  = do
    p <- readArray arr (x,b)
    q <- readArray arr (y,b)
    writeArray arr (x,b) (weightx*p+(1-weightx)*q)
    writeArray arr (b,x) (weightx*p+(1-weightx)*q)

-- Find the minimum row and column of an array
getMin :: IOUArray (Int,Int) Double -> (Int,Int) -> [(Int,Int)] -> IO (Int,Int)
getMin arr cmin (x:xs) = do
    p <- readArray arr x
    q <- readArray arr cmin
    case p < q of
        False -> getMin arr cmin xs
        True -> getMin arr x xs
getMin _ cmin _ = return cmin
