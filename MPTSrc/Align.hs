{- **************************************************************************
*   MP-T has been accepted for publication in Bioinformatics                *
*   The manuscript can be found at: 	doi: 10.1093/bioinformatics/bts640  *
*									    *
*   MP-T is distributed in the hope that it will be useful, but WITHOUT     *
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      *
*   or FITNESS FOR A PARTICULAR PURPOSE. 				    *
*									    *
*   Please report any bugs to hill@stats.ox.ac.uk			    *
*   Suggestions for algorithmic and performance improvements are welcome    *
*   (especially when accompanied by revised code)			    *
*									    *
***************************************************************************-}	
module Align (alignProfiles,needwu,endPoint) where
import Data.Array.IO
import Data.Array.Unboxed
import Profiles (Tree(..),Profile(..))
import Data.Array.Base (unsafeWrite, unsafeRead)
import Control.Monad (foldM)
import LibraryExtension (combineLibEntry,Mappy)

type Pos  = (Int,Int,Int)
type Occupy = UArray Int Int

-- General alignment of two profiles
alignProfiles :: Profile -> Profile -> IOUArray (Int,Int,Int) Int  -> Mappy -> IO (Profile,Mappy)
alignProfiles prof1 prof2 submat library = do
    value <- needwu submat
    let seqd  = updateSeq          (seqs prof1)     (seqs prof2)     value
        key   = assistant equalled (keyToAll prof1) (keyToAll prof2) value
	(ms,lib) = teaLady library (matches prof1)  (matches prof2)  value
	ns    = assistant (+)      (normish prof1)  (normish prof2)  value
	order = (seqOrdering prof1) ++ (seqOrdering prof2)
    return (Profile key ms ns seqd order,lib)


-- ***************************************************** --
--       Functions to make a new profile from            --
--          an old one and a list of match               --
--                  and gap pairs.                       --
-- ***************************************************** --

-- If no env info for seq1, use seq2 info (which may be as bad)
equalled :: Int -> Int -> Int
equalled a b = if a == 1 then b else a

-----------------------------------------------------------------------------
-- **** UGLY hacked together code to do T-Coffee-ing more efficiently **** --
-----------------------------------------------------------------------------
teaLady :: Mappy -> Occupy -> Occupy -> [(Bool,Bool)] -> (Occupy, Mappy)
teaLady library item1 item2 x = (listArray (1,l) (reverse acc),library')
    where
	l = length x
        k1 = elems item1
        k2 = elems item2
        (acc,library') = (doStuff library [] k1 k2 x)

-- The key update function takes two lists and a set of bools. It makes a comparison
-- between the lists and then chooses to use information from one or the other.
doStuff :: Mappy -> [Int] -> [Int] -> [Int] -> [(Bool,Bool)] -> ([Int],Mappy)
doStuff library acc k1 [] _ = ((reverse k1) ++ acc,library)
doStuff library acc [] k2 _ = ((reverse k2) ++ acc,library)
doStuff library acc (k1:ks1) (k2:ks2) (x:xs)
    | x==(True,True)  = doStuff (combineLibEntry library k1 k2) (k1:acc) ks1 ks2 xs
    | fst x == True   = doStuff library (k1:acc) ks1 (k2:ks2) xs
    | otherwise       = doStuff library (k2:acc) (k1:ks1) ks2 xs
doStuff library acc _ _ _ = (acc,library)


-----------------------------------------------------------------------------
-- **** 		Pleasanter code from happier times	      **** --
-----------------------------------------------------------------------------

assistant :: (IArray a e) =>
               (e -> e -> e) -> a Int e -> a Int e -> [(Bool, Bool)] -> a Int e
assistant f item1 item2 x = listArray (1,l) acc
    where
	l = length x
        k1 = elems item1
        k2 = elems item2
        acc = updater f k1 k2 x -- reverse (updater f [] k1 k2 x)

-- The key update function takes two lists and a set of bools. It makes a comparison
-- between the lists and then chooses to use information from one or the other.
updater :: (a -> a -> a) -> [a] -> [a] -> [(Bool, Bool)] -> [a]
updater _ k1 [] _ = k1 
updater _ [] k2 _ = k2
updater f (k1:ks1) (k2:ks2) (x:xs)
    | x==(True,True)  = (f k1 k2) : updater f ks1 ks2 xs
    | fst x == True   = k1 : updater f ks1 (k2:ks2) xs
    | otherwise       = k2 : updater f (k1:ks1) ks2 xs
updater _ _ _ _ = []

updateSeq :: Tree ([Int],[Int]) -> Tree ([Int],[Int]) -> [(Bool,Bool)] -> Tree ([Int],[Int])
updateSeq seq1 seq2 x = Node answer seq1 seq2
	where answer = (answerHelp fst [] 0 x, answerHelp snd [] 0 x)

answerHelp :: (a -> Bool) -> [Int] -> Int -> [a] -> [Int]
answerHelp _ acc c [] = reverse (c:acc)
answerHelp f acc c (x:xs)
  | (c >= 0)  && (f x == True)  = answerHelp f acc (c+1) xs
  | (c >= 0)  && (f x == False) = answerHelp f (c:acc) (-1) xs
  | (c <  0)  && (f x == False) = answerHelp f acc (c-1) xs
  | otherwise = answerHelp f (c:acc) 1 xs

-- ***************************************************** --
--       Functions to perform an alignment. These        --
--    build a traceback matrix, and find the best path   --
--                    back through it                    --
-- ***************************************************** --

alignAssist :: [(Bool,Bool)] -> IOUArray Pos Int -> Pos -> IO [(Bool,Bool)]
alignAssist acc arr (i,j,k)
	| i == 0 = return (take j (repeat (False,True)) ++ acc)
	| j == 0 = return (take i (repeat (True,False)) ++ acc)
	| k == 0 = do
		p1 <- readArray arr (i-1,j-1,-1)
		p2 <- readArray arr (i-1,j-1, 0)
		p3 <- readArray arr (i-1,j-1, 1)
		case (p2 >= p1) && (p2 >= p3) of
		  True -> alignAssist ((True,True):acc) arr (i-1,j-1,0)
		  False -> case (p3 >= p1) of
			True -> alignAssist ((True,True):acc) arr (i-1,j-1,  1)
			False -> alignAssist ((True,True):acc) arr (i-1,j-1,-1)
	| k == -1 = do
		p1 <- readArray arr (i-1,j,-1)
		p2 <- readArray arr (i-1,j, 0)
		p3 <- readArray arr (i-1,j, 1)
		case (p2 >= p1) && (p2 >= p3) of -- *
		  True -> alignAssist ((True,False):acc) arr (i-1,j,0) 
		  False -> case (p1 >= p3) of    -- *
			True -> alignAssist ((True,False):acc) arr (i-1,j,-1) 
			False -> alignAssist ((True,False):acc) arr (i-1,j,1)
	| k == 1 = do
		p1 <- readArray arr (i,j-1,-1)
		p2 <- readArray arr (i,j-1, 0)
		p3 <- readArray arr (i,j-1, 1)
		case (p3 >= p1) && (p3 >= p2 ) of -- *
		  True -> alignAssist ((False,True):acc) arr (i,j-1, 1) 
		  False -> case (p2 >= p1) of
			True -> alignAssist ((False,True):acc) arr (i,j-1,0) 
			False -> alignAssist ((False,True):acc) arr (i,j-1,-1)
alignAssist _ _ (_,_,_) = error "Problem during traceback of alignment"

needwu :: IOUArray (Int,Int,Int) Int -> IO [(Bool,Bool)]
needwu arr = do
    (_,(m,n,_)) <- getBounds arr
    let idxer (i,j,k) = 3*(n+1)*i + 3*j + k + 1
	termPen = 5
    unsafeWrite arr 1 0 -- Set (0,0,0) element to 0
    mapM_ (\(x1,x2) -> unsafeWrite arr x1 x2)
       ([((idxer (0,j,0)),-32768) | j<-[1..n]] ++
        [((idxer (i,0,0)),-32768) | i<-[1..m]] ++
        [((idxer (0,j,-1)),-32768) | j<-[0..n]] ++
        [((idxer (i,0,-1)),-termPen*i) | i<-[1..m]] ++
        [((idxer (0,j,1)),-termPen*j) | j<-[1..n]] ++
        [((idxer (i,0,1)),-32768) | i<-[0..m]])
    sequence_ [updateArrayStart arr i n | i<-[1..m]]
    p <- endPoint arr m n termPen $ [(a,n,c)| a<-[1..m], c<-[-1,0,1]]++[(m,b,c) | b<-[1..n], c<-[-1,0,1]]
    acc <- alignAssist [] arr p
    return $! acc ++ overhang m n p

-- **** Functions to find the maximum value in the traceback matrix **** --
endPoint :: IOUArray Pos Int -> Int -> Int -> Int -> [Pos] -> IO Pos
endPoint dij m n termPen xs = foldM (\x y -> assess dij m n termPen x y) (0,1,0) xs

assess :: IOUArray Pos Int -> Int -> Int -> Int -> Pos -> Pos -> IO Pos
assess d m n termPen (a,b,c) (i,j,k) = do
   let pen = termPen * (i-a) + termPen * (j-b) -- Relative term gap pen between (a,b,c) and (i,j,k)
   candidate <- readArray d (i,j,k)
   current   <- readArray d (a,b,c)
   case compare (candidate + pen) current of
	   GT -> return (i,j,k)
	   LT -> return (a,b,c)
	   -- If there is no clear winner, pick the point which gives the greatest aligned fraction
	   EQ -> case ((intDiv a m) + (intDiv b n)) > ((intDiv i m) + (intDiv j n)) of
	            True -> return (a,b,c)
	            False-> return (i,j,k)
-- ********************************************************************** --

intDiv :: Int -> Int -> Double
intDiv a b = fromIntegral a / fromIntegral b

-- If starting traceback from a place other than the bottom right hand corner
-- of the traceback matrix, add on an appropriate number of match/gap states
overhang :: Int -> Int -> Pos -> [(Bool,Bool)]
overhang m n (i,j,_) = lotsOf (n-j) (False,True) ++ lotsOf (m-i) (True,False)
	where lotsOf x = (take x).repeat

-- Initialise two variables so that we can fill out a column of the traceback
-- efficiently by recursion
updateArrayStart :: IOUArray Pos Int -> Int -> Int ->IO ()
updateArrayStart arr i n = do
    u1 <- readArray arr (i-1,0,0)
    d2 <- readArray arr (i-1,0,-1)
    updateArray arr u1 d2 (i,1) n

max3 :: Int -> Int -> Int -> Int
max3 x y z = max x (max y z)

-- Align a column. The matrix 'arr' initially contains substitution scores for
-- matching (i,j) in each element. As the alignment proceeds these are replaced
-- by the traceback values. Somehow this seems to be more efficient than calculating
-- the substitution scores on the fly.
updateArray :: IOUArray (Int,Int,Int) Int ->
                    Int -> Int -> (Int,Int) -> Int -> IO ()
updateArray arr u1 d2 (i,j) n = do
    let idxer (f1,f2,f3) = 3*(n+1)*f1 + 3*f2 + f3 +1 :: Int
        idx = idxer (i,j,-1)
    d1 <- unsafeRead arr (idxer (i-1,j,-1))
    s1 <- unsafeRead arr (idxer (i-1,j,0))
    i3 <- unsafeRead arr (idxer (i-1,j,1))
    unsafeWrite arr idx (max3 s1 d1 i3)
    i1 <- unsafeRead arr (idxer (i,j-1,1))
    t1 <- unsafeRead arr (idxer (i,j-1,0))
    d3 <- unsafeRead arr (idxer (i,j-1,-1))
    unsafeWrite arr (idx+2) (max3 t1 i1 d3)
    i2 <- unsafeRead arr (idxer (i-1,j-1,1))
    sub <- unsafeRead arr (idx+1)      -- (idxer (i,j,0))
    unsafeWrite arr (idx+1) (sub+(max3 u1 i2 d2))
    case j<n of
        True -> updateArray arr s1 d1 (i,j+1) n
        False ->return ()
