README 26 Sep 12

This package contains the MP-T sequence-structure alignment program, and a set of helper scripts which can be used to automate homolog selection.

There are two ways of making an alignment:

1) If you wish to provide MP-T with homologous sequences that you have selected yourself, use the bin/MPT executable. Read README_MPT for usage details.

2) If you have just a target sequence and a template structure, and do not wish to collect homologous sequences yourself, use the MPTpipeline script. This takes as input a directory containing files named structure.tem and sequence.fasta, automatically selects homologs, and produces a multiple sequence alignment as output. Set-up details can be found in README_HOMOLOG. 

You should be able to make an alignment by typing:

bin/MPTpipeline example /path/to/a/blast/database

If all goes well a file called MPT_output.fasta will be made in the target directory. 
